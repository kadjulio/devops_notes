## Elasticsearch cluster with SSL/TLS
### Install ES cluster

* sudo apt-get update && sudo apt-get upgrade -y

* sudo apt install default-jre -y

* java --version

* wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

* sudo apt-get install apt-transport-https

* echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list

* sudo apt-get update && sudo apt-get install elasticsearch

* sudo /bin/systemctl daemon-reload && sudo /bin/systemctl enable elasticsearch.service

* sudo systemctl start elasticsearch.service

### Config ES cluster
```
vi /etc/elasticsearch/elasticsearch.yml

# ------------------------------------ Node ------------------------------------
node.name: es-node01	        
node.roles: [ master, data ]  
#
# ---------------------------------- Network -----------------------------------
network.host: 10.0.3.11	
http.port: 9200					
#
# ---------------------------------- Cluster -----------------------------------
cluster.name: es_cluster                                             
cluster.initial_master_nodes: ["es-node01","es-node02","es-node03"]  
#
# --------------------------------- Discovery ----------------------------------
discovery.seed_hosts: ["10.0.3.11", "10.0.3.12", "10.0.3.13"]                       
#
# ----------------------------------- Paths ------------------------------------
path.data: /var/lib/elasticsearch 
path.logs: /var/log/elasticsearch 
# ----------------------------------- Memory -----------------------------------
bootstrap.memory_lock: true
```

### Config Elasticseach options
```
vim /etc/default/elasticsearch
MAX_LOCKED_MEMORY=unlimited

-----------------------------------------------------------------
vim /etc/sysctl.conf
vm.max_map_count=262144

-----------------------------------------------------------------
vim /etc/security/limits.conf
elasticsearch - nofile 65536
elasticsearch soft memlock unlimited
elasticsearch hard memlock unlimited

-----------------------------------------------------------------
vim /etc/pam.d/su
session    required   pam_limits.so

-----------------------------------------------------------------
vim /etc/systemd/system/elasticsearch.service.d/override.conf
- OR -
systemctl edit elasticsearch

[Service]
LimitMEMLOCK=infinity
```

### Check node

curl -XGET 'http://localhost:9200/_cluster/health?pretty'
curl -XGET 'http://localhost:9200/_cluster/state?pretty'
curl -sS -XGET "http://localhost:9200/_cat/master?pretty" |& tee /tmp/the_master_node_was_here

### Create indexes

curl -X PUT http://172.31.17.206:9200/syslogs/ -H 'Content-Type: application/json' -d '{
  "settings": {
    "index": {
      "number_of_shards": 3,  
      "number_of_replicas": 2 
    }
  }
}'

curl -X PUT http://172.31.17.206:9200/applogs/ -H 'Content-Type: application/json' -d '{
  "settings": {
    "index": {
      "number_of_shards": 3,  
      "number_of_replicas": 2 
    }
  }
}'


curl -X GET http://localhost:9200/_cat/indices?v
